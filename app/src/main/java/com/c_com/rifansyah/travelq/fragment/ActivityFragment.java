package com.c_com.rifansyah.travelq.fragment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.c_com.rifansyah.travelq.HomeActivity;
import com.c_com.rifansyah.travelq.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class ActivityFragment extends Fragment {
    View rootView;
    public ActivityFragment() {
        // Required empty public constructor
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_activity, container, false);

        showNotif();
        return rootView;
    }

    private void showNotif(){
        NotificationManager notificationManager;
        Intent mIntent = new Intent(getActivity(), HomeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("fromnotif", "notif");
        mIntent.putExtras(bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity());
        builder.setColor(getResources().getColor(R.color.colorAccent));
        builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.small_icon_notif)
                .setTicker("Travel Q : your trip to Yogyakarta will begin in 7 days")
                .setLargeIcon(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.icon_notif))
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentTitle("Travel Q")
                .setContentText("your trip to Yogyakarta will begin in 7 days");

        notificationManager = (NotificationManager) getActivity().getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(115, builder.build());
    }
}
