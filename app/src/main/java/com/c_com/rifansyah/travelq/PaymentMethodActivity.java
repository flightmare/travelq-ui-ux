package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

public class PaymentMethodActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Payment");
        getSupportActionBar().setSubtitle("13 May 2017");
        setContentView(R.layout.activity_payment_method);

        final RadioButton alfamartButton = (RadioButton)findViewById(R.id.radio_alfamart_paymentmethod);
        final RadioButton indomartButton = (RadioButton)findViewById(R.id.radio_indomart_paymentmethod);
        final RadioButton atmButton = (RadioButton)findViewById(R.id.radio_atm_paymentmethod);

        alfamartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alfamartButton.setChecked(true);
                indomartButton.setChecked(false);
                atmButton.setChecked(false);
            }
        });

        indomartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alfamartButton.setChecked(false);
                indomartButton.setChecked(true);
                atmButton.setChecked(false);
            }
        });

        atmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alfamartButton.setChecked(false);
                indomartButton.setChecked(false);
                atmButton.setChecked(true);
            }
        });

        Button continueButton = (Button)findViewById(R.id.button_continue_paymentmethod);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentMethodActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
