package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ListFlightActivity extends AppCompatActivity {
    RelativeLayout layoutCitilink, layoutLion, layoutBatik;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_flight);
        getSupportActionBar().setSubtitle("13 May 2017");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        layoutCitilink = (RelativeLayout) findViewById(R.id.layout_citilink_listflight);
        layoutBatik = (RelativeLayout)findViewById(R.id.layout_batik_listflight);
        layoutLion = (RelativeLayout)findViewById(R.id.layout_lion_listflight);
        final Intent intent = getIntent();
        String title = "first";
        String origin = "first";
        try{
            title = intent.getStringExtra("title");
            origin = intent.getStringExtra("origin");

            if(origin.equalsIgnoreCase("Jakarta (JKTA)")){
                getSupportActionBar().setTitle("JKTA-YOG");
            } else if(origin.equalsIgnoreCase("Yogyakarta (YOG)")){
                getSupportActionBar().setTitle("YOG-JKTA");
            }
        }catch (Exception e){

        }
        switch (title){
            case "first":
                layoutCitilink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent1 = new Intent(ListFlightActivity.this, FlightConfirmationActivity.class);
                        intent1.putExtra("title","first");
                        startActivity(intent1);
                    }
                });
                break;
            case "true":
                layoutCitilink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent1 = new Intent(ListFlightActivity.this, FlightConfirmationActivity.class);
                        intent1.putExtra("title","true");
                    //    intent1.putExtra("origin", "Yogyakarta (YOG)");
                        startActivity(intent1);
                    }
                });
                break;
            case "false":
                layoutCitilink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent1 = new Intent(ListFlightActivity.this, FlightConfirmationActivity.class);
                        intent1.putExtra("title","false");
                        startActivity(intent1);
                    }
                });
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
