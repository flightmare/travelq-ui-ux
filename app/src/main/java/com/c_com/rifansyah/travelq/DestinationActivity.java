package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.andexert.library.RippleView;

public class DestinationActivity extends AppCompatActivity {
    RippleView rippleView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        rippleView = (RippleView)findViewById(R.id.search_destination);
        final Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        getSupportActionBar().setTitle(title);

        if(title.equalsIgnoreCase("quick tour")){
            rippleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(DestinationActivity.this, QuickTourActivity.class);
                    intent1.putExtra("title","destination");
                    startActivity(intent1);
                }
            });
        } else if(title.equalsIgnoreCase("Tour Package")){
            rippleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(DestinationActivity.this, TourPackageActivity.class);
                    startActivity(intent1);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
