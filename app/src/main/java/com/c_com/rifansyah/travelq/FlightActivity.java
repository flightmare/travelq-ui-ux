package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import com.fourmob.datetimepicker.date.DatePickerDialog;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class FlightActivity extends AppCompatActivity implements com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener{
    TextView textReturnDate;
    LinearLayout layoutReturnDate;
    Switch switchRoundTrip;
    TextView plusAdultButton, plusKidsButton, plusBabyButton, minAdultButton, minKidsButton, minBabyButton;
    TextView textCounterAdult, textCounterKids, textCounterBaby;
    ImageButton reverseButton;
    TextView textOrigin, textDestination;
    TextView textReturnDateFlight;
    TextView textDepartureDateFlight;
    Button continueButton;
    boolean departure = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search Plane");

        //set date
        final Calendar c = Calendar.getInstance();
        System.out.println("Current time => "+c.getTime());

        SimpleDateFormat[] df = {new SimpleDateFormat("dd-MM-yyyy")};
        String formattedDate = df[0].format(c.getTime());
        SimpleDateFormat[] day = {new SimpleDateFormat("EE")};
        String dayName = day[0].format(c.getTime());

        String[] date = formattedDate.split("-");
        final int dayDate = Integer.parseInt(date[0]);
        final int monthDate = Integer.parseInt(date[1]) - 1;
        final int yearDate = Integer.parseInt(date[2]);

        df[0] = new SimpleDateFormat("MMM");
        String monthName = df[0].format(c.getTime());

        //get intent
        Intent intent = getIntent();
        String title = "first";
        try{
            title = intent.getStringExtra("title");
        }catch (Exception e){

        }

        textOrigin = (TextView)findViewById(R.id.text_origin_flight);
        textDestination = (TextView)findViewById(R.id.text_destination_flight);
        textReturnDate = (TextView)findViewById(R.id.text_return_date);
        layoutReturnDate = (LinearLayout)findViewById(R.id.layout_return_date_flight);
        textReturnDateFlight = (TextView)findViewById(R.id.text_return_date_flight);
        textReturnDate.setVisibility(View.GONE);
        layoutReturnDate.setVisibility(View.GONE);
        textDepartureDateFlight = (TextView)findViewById(R.id.text_departure_date_flight);
        textDepartureDateFlight.setText(dayName + ", " + dayDate + " " + monthName + " " + yearDate);

        //boolean
        final boolean depart = false;
        textDepartureDateFlight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Date Picker
                com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(FlightActivity.this, yearDate, monthDate, dayDate);
                departure = true;
                datePickerDialog.show(getSupportFragmentManager(), "TAG_DATE_DEPARTURE_PICKER_DIALOG");

            }
        });
        layoutReturnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get date
                Calendar c = Calendar.getInstance();
                System.out.println("Current time => "+c.getTime());

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String formattedDate = df.format(c.getTime());
                String[] date = formattedDate.split("-");
                int dayDate = Integer.parseInt(date[2]);
                int mounthDate = Integer.parseInt(date[1]) - 1;
                int yearDate = Integer.parseInt(date[0]);

                df = new SimpleDateFormat("MM");
                String mounthName = df.format(c.getTime());

                //Date Picker
                com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(FlightActivity.this, yearDate, mounthDate, dayDate);
                departure = false;
                datePickerDialog.show(getSupportFragmentManager(), "TAG_DATE_RETURN_PICKER_DIALOG");
            }
        });
        switchRoundTrip = (Switch)findViewById(R.id.switch_flight);
        switchRoundTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(switchRoundTrip.isChecked()){
                    textReturnDate.setVisibility(View.VISIBLE);
                    layoutReturnDate.setVisibility(View.VISIBLE);
                }else{
                    textReturnDate.setVisibility(View.GONE);
                    layoutReturnDate.setVisibility(View.GONE);
                }
            }
        });

        continueButton = (Button)findViewById(R.id.button_continue_flight);
        final String finalTitle = title;
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FlightActivity.this, ListFlightActivity.class);
                String roundTripTemp = String.valueOf(switchRoundTrip.isChecked());
                if(finalTitle.equalsIgnoreCase("first")){
                    intent.putExtra("title","first");
                } else {
                    intent.putExtra("title", roundTripTemp);
                }
                intent.putExtra("origin", (String)textOrigin.getText());
                startActivity(intent);
            }
        });

        plusAdultButton = (TextView)findViewById(R.id.plus_adult_button);
        plusKidsButton = (TextView)findViewById(R.id.plus_kids_button);
        plusBabyButton = (TextView)findViewById(R.id.plus_baby_button);
        minAdultButton = (TextView)findViewById(R.id.min_adult_button);
        minKidsButton = (TextView)findViewById(R.id.min_kids_button);
        minBabyButton = (TextView)findViewById(R.id.min_baby_button);

        textCounterAdult = (TextView)findViewById(R.id.text_counter_adult_flight);
        textCounterKids = (TextView)findViewById(R.id.text_counter_kids_flight);
        textCounterBaby = (TextView)findViewById(R.id.text_counter_baby_flight);

        plusAdultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt((String) textCounterAdult.getText());
                count++;
                textCounterAdult.setText(count + "");
            }
        });

        plusKidsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt((String) textCounterKids.getText());
                count++;
                textCounterKids.setText(count + "");
            }
        });

        plusBabyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt((String) textCounterBaby.getText());
                count++;
                textCounterBaby.setText(count + "");
            }
        });

        minAdultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt((String) textCounterAdult.getText());
                if(count > 0){
                    count--;
                }
                textCounterAdult.setText(count + "");
            }
        });

        minKidsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt((String) textCounterKids.getText());
                if(count > 0){
                    count--;
                }
                textCounterKids.setText(count + "");
            }
        });

        minBabyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt((String) textCounterBaby.getText());
                if(count > 0){
                    count--;
                }
                textCounterBaby.setText(count + "");
            }
        });

        reverseButton = (ImageButton)findViewById(R.id.reverse_button_flight);
        reverseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String origin = (String) textOrigin.getText();
                String destination = (String) textDestination.getText();
                textOrigin.setText(destination);
                textDestination.setText(origin + "");
            }
        });
    }
    @Override
    public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
        month++;
        if(departure == false){
            String dateString = String.format("%d-%d-%d", year, month, day);
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-M-d").parse(dateString);
            } catch (ParseException e) {

            }

            String dayOfWeek = new SimpleDateFormat("EE", Locale.ENGLISH).format(date);
            String monthOfYear = new SimpleDateFormat("MMM", Locale.ENGLISH).format(date);
            textReturnDateFlight.setText(dayOfWeek + ", " + day + " " + monthOfYear + " " + year);
        } else if(departure == true){
            String dateString = String.format("%d-%d-%d", year, month, day);
            Date date = null;
            try {
                date = new SimpleDateFormat("yyyy-M-d").parse(dateString);
            } catch (ParseException e) {

            }

            String dayOfWeek = new SimpleDateFormat("EE", Locale.ENGLISH).format(date);
            String monthOfYear = new SimpleDateFormat("MMM", Locale.ENGLISH).format(date);
            textDepartureDateFlight.setText(dayOfWeek + ", " + day + " " + monthOfYear + " " + year);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
