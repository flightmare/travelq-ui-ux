package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.c_com.rifansyah.travelq.fragment.quicktour.PriceFragment;
import com.c_com.rifansyah.travelq.fragment.quicktour.WishlistFragment;

public class QuickTourActivity extends AppCompatActivity {
    WishlistFragment wishlistFragment;
    PriceFragment priceFragment;
    Button wishlistButton, priceButton;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wishlistFragment = new WishlistFragment();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Quick Tour");

        //fragment
        setContentView(R.layout.activity_quick_tour);

        final View viewBuy = (View)findViewById(R.id.view_buy);
        final View viewSell = (View)findViewById(R.id.view_sell);

        final TextView buy = (TextView)findViewById(R.id.buy_transaction);
        final TextView sell = (TextView)findViewById(R.id.sell_transaction);

        sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                priceFragment = new PriceFragment();
                sell.setTextColor(Color.parseColor("#ffffff"));
                buy.setTextColor(Color.parseColor("#90ffffff"));
                viewBuy.setBackground(new ColorDrawable(Color.parseColor("#044174")));
                viewSell.setBackground(new ColorDrawable(Color.parseColor("#ffffff")));
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_quick_tour, priceFragment);
                transaction.commit();
            }
        });

        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell.setTextColor(Color.parseColor("#90ffffff"));
                buy.setTextColor(Color.parseColor("#ffffff"));
                viewBuy.setBackground(new ColorDrawable(Color.parseColor("#ffffff")));
                viewSell.setBackground(new ColorDrawable(Color.parseColor("#044174")));
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_quick_tour, wishlistFragment);
                transaction.commit();
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_quick_tour, wishlistFragment );
        transaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(QuickTourActivity.this, HomeActivity.class);
                startActivity(intent);
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(QuickTourActivity.this, HomeActivity.class);
        startActivity(intent);
        this.finish();
    }
}
