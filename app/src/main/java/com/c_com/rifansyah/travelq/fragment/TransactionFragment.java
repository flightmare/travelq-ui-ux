package com.c_com.rifansyah.travelq.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.c_com.rifansyah.travelq.R;
import com.c_com.rifansyah.travelq.fragment.transaction.BuyFragment;
import com.c_com.rifansyah.travelq.fragment.transaction.SellFragment;

public class TransactionFragment extends Fragment {
    public TransactionFragment(){

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_transaction, container, false);
//first fragment
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_transaction, new BuyFragment());
        transaction.commit();

        final View viewBuy = (View)rootView.findViewById(R.id.view_buy);
        final View viewSell = (View)rootView.findViewById(R.id.view_sell);

        final TextView buy = (TextView)rootView.findViewById(R.id.buy_transaction);
        final TextView sell = (TextView)rootView.findViewById(R.id.sell_transaction);
        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell.setTextColor(Color.parseColor("#90ffffff"));
                buy.setTextColor(Color.parseColor("#ffffff"));
                viewBuy.setBackground(new ColorDrawable(Color.parseColor("#ffffff")));
                viewSell.setBackground(new ColorDrawable(Color.parseColor("#044174")));

                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_transaction, new BuyFragment());
                transaction.commit();
            }
        });
        sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sell.setTextColor(Color.parseColor("#ffffff"));
                buy.setTextColor(Color.parseColor("#90ffffff"));
                viewBuy.setBackground(new ColorDrawable(Color.parseColor("#044174")));
                viewSell.setBackground(new ColorDrawable(Color.parseColor("#ffffff")));

                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_transaction, new SellFragment());
                transaction.commit();
            }
        });
        return rootView;
    }
}
