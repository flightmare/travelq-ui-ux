package com.c_com.rifansyah.travelq;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TourPackageDescriptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Tour Package");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_tour_package_description);
    }
}
