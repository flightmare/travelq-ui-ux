package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class FlightConfirmationActivity extends AppCompatActivity {
    Button continueButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_flight_confirmation);
        continueButton = (Button)findViewById(R.id.button_continue_flightconfirmation);
        String title = "first";
        try{
            Intent intent = getIntent();
            title = intent.getStringExtra("title");
        }catch (Exception e){

        }

        switch (title){
            case "first":
                Intent intent1 = new Intent(FlightConfirmationActivity.this, PaymentMethodActivity.class);
                startActivity(intent1);
                break;
            case "true":
                continueButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent1 = new Intent(FlightConfirmationActivity.this, ListFlightActivity.class);
                        intent1.putExtra("title","false");
                        intent1.putExtra("origin", "Yogyakarta (YOG)");
                        startActivity(intent1);
                    }
                });
                break;
            case "false":
                continueButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent1 = new Intent(FlightConfirmationActivity.this, QuickTourActivity.class);
                        intent1.putExtra("title","listflight");
                        startActivity(intent1);
                    }
                });
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
