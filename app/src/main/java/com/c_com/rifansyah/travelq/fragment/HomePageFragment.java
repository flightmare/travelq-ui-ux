package com.c_com.rifansyah.travelq.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.andexert.library.RippleView;
import com.c_com.rifansyah.travelq.DestinationActivity;
import com.c_com.rifansyah.travelq.FlightActivity;
import com.c_com.rifansyah.travelq.HomeActivity;
import com.c_com.rifansyah.travelq.HotelActivity;
import com.c_com.rifansyah.travelq.ListFlightActivity;
import com.c_com.rifansyah.travelq.QuickTourActivity;
import com.c_com.rifansyah.travelq.R;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;


public class HomePageFragment extends Fragment implements View.OnClickListener {
    int[] img = {R.drawable.mqdefault, R.drawable.header1, R.drawable.header2};
    CarouselView carouselView;
    RippleView rippleView;
    // TODO: Rename and change types of parameters
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_page, container, false);
        carouselView = (CarouselView)rootView.findViewById(R.id.carouselView);
        carouselView.setPageCount(img.length);

        carouselView.setImageListener(imageListener);

        rippleView = (RippleView)rootView.findViewById(R.id.quick_tour_view);
        rippleView.setOnClickListener(this);
        RippleView tourPackageLayout = (RippleView)rootView.findViewById(R.id.tour_package_view);
        tourPackageLayout.setOnClickListener(this);
        RippleView hotelView = (RippleView)rootView.findViewById(R.id.hotel_view);
        hotelView.setOnClickListener(this);
        RippleView planeView = (RippleView)rootView.findViewById(R.id.flight_view_homepage);
        planeView.setOnClickListener(this);

        return rootView;
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(img[position]);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.quick_tour_view:
                Intent intent = new Intent(getActivity(), DestinationActivity.class);
                intent.putExtra("title","Quick Tour");
                startActivity(intent);
                break;
            case R.id.flight_view_homepage:
                intent = new Intent(getActivity(), FlightActivity.class);
                intent.putExtra("title", "first");
                startActivity(intent);
                break;
            case R.id.hotel_view:
                intent = new Intent(getActivity(), HotelActivity.class);
                intent.putExtra("title", "first");
                startActivity(intent);
                break;
            case R.id.tour_package_view:
                intent = new Intent(getActivity(), DestinationActivity.class);
                intent.putExtra("title","Tour Package");
                startActivity(intent);
                break;
        }
    }
}
