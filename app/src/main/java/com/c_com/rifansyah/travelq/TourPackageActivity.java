package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

public class TourPackageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Tour Package");
        setContentView(R.layout.activity_tour_package);

        LinearLayout lavaTour = (LinearLayout)findViewById(R.id.layout_lavatourmerapi_tourpackage);
        LinearLayout budayaJogjaTur = (LinearLayout)findViewById(R.id.layout_turbudayajogja_tourpackage);
        LinearLayout pantaiTur = (LinearLayout)findViewById(R.id.layout_tourpantai_tourpackage);
        budayaJogjaTur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TourPackageActivity.this, TourPackageDescriptionActivity.class);
                startActivity(intent);
            }
        });
        pantaiTur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TourPackageActivity.this, TourPackageDescriptionActivity.class);
                startActivity(intent);
            }
        });
        lavaTour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TourPackageActivity.this, TourPackageDescriptionActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
