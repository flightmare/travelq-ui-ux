package com.c_com.rifansyah.travelq.fragment.quicktour;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.c_com.rifansyah.travelq.DestinationActivity;
import com.c_com.rifansyah.travelq.R;
import com.c_com.rifansyah.travelq.*;
import com.sa90.materialarcmenu.ArcMenu;

/**
 * A simple {@link Fragment} subclass.
 */
public class WishlistFragment extends Fragment {
    ImageButton destinationWishlist, planeWishlist, hotelWishlist;
    String title;

    public WishlistFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);
        planeWishlist = (ImageButton)view.findViewById(R.id.plane_wishlist_arc);
        planeWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(planeWishlist.getTag().toString().equalsIgnoreCase("enable")) {
                    Intent intent = new Intent(getActivity(), FlightActivity.class);
                    intent.putExtra("title","quicktour");
                    startActivity(intent);
                }
            }
        });
        hotelWishlist = (ImageButton)view.findViewById(R.id.hotel_wishlist_arc);
        hotelWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hotelWishlist.getTag().toString().equalsIgnoreCase("enable")) {
                    Intent intent = new Intent(getActivity(), HotelActivity.class);
                    intent.putExtra("title","quicktour");
                    startActivity(intent);
                }
            }
        });
        destinationWishlist = (ImageButton)view.findViewById(R.id.destination_wishlist);
        destinationWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DestinationActivity.class);
                intent.putExtra("title","Quick Tour");
                startActivity(intent);
            }
        });
        title = "first";
        try{
            title = ((QuickTourActivity)getActivity()).getIntent().getExtras().getString("title");
        }catch (Exception e){
            title = "first";
        }
        switch (title){
            case "first":
                LinearLayout destination = (LinearLayout)view.findViewById(R.id.destination_layout);
                LinearLayout plane = (LinearLayout)view.findViewById(R.id.plane_view_wishlist);
                LinearLayout hotel = (LinearLayout)view.findViewById(R.id.hotel_wishlist_view);
                destination.setVisibility(View.GONE);
                plane.setVisibility(View.GONE);
                hotel.setVisibility(View.GONE);
                LinearLayout noItem = (LinearLayout)view.findViewById(R.id.noitem_text);
                noItem.setVisibility(View.VISIBLE);
                ImageView hotelButton = (ImageView)view.findViewById(R.id.hotel_wishlist_arc);
                ImageButton planeButton = (ImageButton)view.findViewById(R.id.plane_wishlist_arc);
                ImageButton trainButton = (ImageButton)view.findViewById(R.id.train_wishlist_arc);
                planeButton.setImageResource(R.drawable.plane_unused_dialer);
                planeButton.setTag("disable");
                trainButton.setImageResource(R.drawable.train_unused_dialer);
                hotelButton.setImageResource(R.drawable.hotel_unactive);
              break;
            case "destination":
                destination = (LinearLayout)view.findViewById(R.id.destination_layout);
                plane = (LinearLayout)view.findViewById(R.id.plane_view_wishlist);
                hotel = (LinearLayout)view.findViewById(R.id.hotel_wishlist_view);
                destination.setVisibility(View.VISIBLE);
                plane.setVisibility(View.GONE);
                hotel.setVisibility(View.GONE);
                noItem = (LinearLayout)view.findViewById(R.id.noitem_text);
                noItem.setVisibility(View.GONE);
                hotelButton = (ImageView)view.findViewById(R.id.hotel_wishlist_arc);
                hotelButton.setImageResource(R.drawable.hotel_dialer);
                hotelButton.setTag("enable");
                planeButton = (ImageButton)view.findViewById(R.id.plane_wishlist_arc);
                planeButton.setTag("enable");
                planeButton.setImageResource(R.drawable.plane_dialer);
                break;
            case "listflight":
                destination = (LinearLayout)view.findViewById(R.id.destination_layout);
                plane = (LinearLayout)view.findViewById(R.id.plane_view_wishlist);
                hotel = (LinearLayout)view.findViewById(R.id.hotel_wishlist_view);
                destination.setVisibility(View.VISIBLE);
                plane.setVisibility(View.VISIBLE);
                hotel.setVisibility(View.GONE);
                noItem = (LinearLayout)view.findViewById(R.id.noitem_text);
                noItem.setVisibility(View.GONE);
                hotelButton = (ImageView)view.findViewById(R.id.hotel_wishlist_arc);
                hotelButton.setImageResource(R.drawable.hotel_dialer);
                planeButton = (ImageButton)view.findViewById(R.id.plane_wishlist_arc);
                planeButton.setTag("enable");
                hotelButton.setTag("enable");
                planeButton.setImageResource(R.drawable.plane_dialer);
                break;
            case "hotel":
                destination = (LinearLayout)view.findViewById(R.id.destination_layout);
                plane = (LinearLayout)view.findViewById(R.id.plane_view_wishlist);
                hotel = (LinearLayout)view.findViewById(R.id.hotel_wishlist_view);
                destination.setVisibility(View.VISIBLE);
                plane.setVisibility(View.VISIBLE);
                hotel.setVisibility(View.VISIBLE);
                noItem = (LinearLayout)view.findViewById(R.id.noitem_text);
                noItem.setVisibility(View.GONE);
                hotelButton = (ImageView)view.findViewById(R.id.hotel_wishlist_arc);
                hotelButton.setImageResource(R.drawable.hotel_dialer);
                planeButton = (ImageButton)view.findViewById(R.id.plane_wishlist_arc);
                planeButton.setTag("enable");
                hotelButton.setTag("enable");
                planeButton.setImageResource(R.drawable.plane_dialer);
                LinearLayout destination2 = (LinearLayout)view.findViewById(R.id.destination2_layout);
                LinearLayout plane2 = (LinearLayout)view.findViewById(R.id.plane2_view_wishlist);
                destination2.setVisibility(View.VISIBLE);
                plane2.setVisibility(View.VISIBLE);
                break;
        }

        return view;
    }

}
