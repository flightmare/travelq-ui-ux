package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

public class ListHotelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Hotel Yogyakarta");
        getSupportActionBar().setSubtitle("13 May 2017");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_list_hotel);
        Intent intent = getIntent();
        String title = "first";
        try{
            title = intent.getStringExtra("title");

        }catch (Exception e){

        }

        RelativeLayout popHotel = (RelativeLayout)findViewById(R.id.layout_pop_listhotel);
        if(title.equalsIgnoreCase("quicktour")){
            popHotel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(ListHotelActivity.this, QuickTourActivity.class);
                    intent1.putExtra("title","hotel");
                    startActivity(intent1);
                }
            });
        } else if(title.equalsIgnoreCase("first")){
            popHotel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent1 = new Intent(ListHotelActivity.this, PaymentMethodActivity.class);
                    startActivity(intent1);
                }
            });
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
