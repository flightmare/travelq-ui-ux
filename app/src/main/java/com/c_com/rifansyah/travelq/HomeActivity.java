package com.c_com.rifansyah.travelq;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;
import android.support.v7.app.ActionBar;
import com.c_com.rifansyah.travelq.fragment.*;
import com.c_com.rifansyah.travelq.fragment.ActivityFragment;
import com.c_com.rifansyah.travelq.fragment.HomePageFragment;
import com.c_com.rifansyah.travelq.fragment.ProfileFragment;
import com.c_com.rifansyah.travelq.fragment.TransactionFragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class HomeActivity extends AppCompatActivity {
    int backButtonCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        setContentView(R.layout.activity_home);
        backButtonCount = 0;
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#10044174")));
     //   getSupportActionBar().setLogo(R.drawable.g4677);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        BottomBar bottomBar = (BottomBar)findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                Fragment selectedFragment = null;
                switch (tabId){
                    case R.id.tab_home:
                        selectedFragment = new HomePageFragment();
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#10044174")));
                        getSupportActionBar().setTitle("TravelQ");
                        break;
                    case R.id.tab_transaction:
                        selectedFragment = new TransactionFragment();
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#044174")));
                        getSupportActionBar().setTitle("Transaction");
                        break;
                    case R.id.tab_activity:
                        selectedFragment = new ActivityFragment();
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#044174")));
                        getSupportActionBar().setTitle("PlanQ");
                        break;
                    case R.id.tab_profile:
                        selectedFragment = new ProfileFragment();
                        getSupportActionBar().setTitle("Profile");
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#044174")));
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_home, selectedFragment);
                transaction.commit();
            }
        });

        //first fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_home, new HomePageFragment());
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.login:
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.tour_list:
                Toast.makeText(this, "wuhuhu", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }
    private Boolean exit = false;
    @Override
    public void onBackPressed()
    {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }
}
