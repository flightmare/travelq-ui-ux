package com.c_com.rifansyah.travelq.fragment.quicktour;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.c_com.rifansyah.travelq.PaymentMethodActivity;
import com.c_com.rifansyah.travelq.QuickTourActivity;
import com.c_com.rifansyah.travelq.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PriceFragment extends Fragment {
    String title;

    public PriceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_price, container, false);
        title = "first";
        try{
            title = ((QuickTourActivity)getActivity()).getIntent().getExtras().getString("title");
        }catch (Exception e){
            title = "first";
        }
        switch (title){
            case "first":
                LinearLayout plane = (LinearLayout)view.findViewById(R.id.plane_view_price);
                LinearLayout hotel = (LinearLayout)view.findViewById(R.id.hotel_view_price);
                TextView total = (TextView)view.findViewById(R.id.total_price);
                plane.setVisibility(View.GONE);
                hotel.setVisibility(View.GONE);
                total.setText("IDR 0");
                break;
            case "listflight":
                plane = (LinearLayout)view.findViewById(R.id.plane_view_price);
                hotel = (LinearLayout)view.findViewById(R.id.hotel_view_price);
                total = (TextView)view.findViewById(R.id.total_price);
                plane.setVisibility(View.VISIBLE);
                hotel.setVisibility(View.GONE);
                total.setText("IDR 550.000");
                break;
            case "hotel":
                plane = (LinearLayout)view.findViewById(R.id.plane_view_price);
                hotel = (LinearLayout)view.findViewById(R.id.hotel_view_price);
                total = (TextView)view.findViewById(R.id.total_price);
                plane.setVisibility(View.VISIBLE);
                hotel.setVisibility(View.VISIBLE);
                break;
            default:
                plane = (LinearLayout)view.findViewById(R.id.plane_view_price);
                hotel = (LinearLayout)view.findViewById(R.id.hotel_view_price);
                total = (TextView)view.findViewById(R.id.total_price);
                plane.setVisibility(View.GONE);
                hotel.setVisibility(View.GONE);
                total.setText("IDR 0");
        }


        Button continueButton = (Button)view.findViewById(R.id.continue_price);
        continueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PaymentMethodActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

}
